# Training

## Documents
This git repo contains three documents that all relate to a set of challenges clearly defined in the challenge document. The goal of these three documents is to create an environment in which one can learn to program in Java, as well as learn the concepts behind Java—object oriented programming. These documents, their structure loosely based of the nature method pedagogy from language acquisition, aim to make this process as self explanatory as possible.

### Challenges
The aforementioned challenges document is a series of tasks that one must complete, structured in the classic input/output format. Rather than including a problem statement, however, this repository gives no context as to any given code. The reason why the code must completed is simply to learn the concept behind the completed code. The challenges are meant to simply reinforce ideas already learned in the concepts document.

### Concepts
The concepts document will contain the bulk of the text and is meant to be where the bulk of the apprehension should occur. The goal of the concepts document is to be mostly programming language agnostic. It should teach programming in an abstract sense, without worrying about the syntax of any particular language.

### Syntax
As programmers still do need to learn the syntax of the programming language the want to use, the syntax document exists. It mostly includes code with output that demonstrate certain features of Java. There are comments explaining certain lines of code, but they are very mimimal. The reader is _meant_ to spend time trying to figure out what part of the code is relevant to them and how to use it. This document aims to make the comments be enough to logically deduce how the code can be generalized, similar to the text in _Lingua Latina per se Illustrata_.

## To-Do
- [x] Output
- [x] Input
- [x] Interactivity
- [x] Variables
- [x] Operators
- [ ] Methods
- [ ] Method Parameters
